const express = require("express");
const path = require("path");

const app = express();

const port = 8000;

app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/view/sample.06restAPI.order.pizza365.v2.0.html"))
})

app.listen(port, () => {
    console.log("App on Port: " + port);
})